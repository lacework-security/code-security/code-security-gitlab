import { spawnSync, spawn } from 'child_process'



function getBooleanInput(name: string): boolean {
    const debug = process.env[name];
    if(debug) {
        return debug.toLowerCase() === 'true'
    }
    return false;
}

export function debug(): boolean {
    return getBooleanInput('LW_DEBUG')
}
 
export function autofix(): boolean {
  return getBooleanInput('LW_AUTOFIX')
}

export async function callCommand(command: string, ...args: string[]) {
  console.info('Invoking ' + command + ' ' + redactApiCredentials(args.join(' ')))
  const child = spawn(command, args, { stdio: 'inherit' })
  const exitCode = await new Promise((resolve, _) => {
    child.on('close', resolve)
  })
  if (exitCode !== 0) {
    console.error(`Command failed with status ${exitCode}`)
    throw new Error(`Command failed with status ${exitCode}`)
  }
}

export function getRequiredEnvVariable(name: string) {
    const value = process.env[name]
    if (!value) {
      console.error(`Missing required environment variable ${name}`)
      process.exit(0) // TODO: Exit with 1 once we want failures to be fatal
    }
    return value
}

export function getGitLabToken(): string {
  const lwGitLabToken = process.env.LW_GITLAB_TOKEN
  const laceworkGitLabToken = process.env.LACEWORK_GITLAB_TOKEN

  if (lwGitLabToken) {
    return lwGitLabToken
  }
  if (laceworkGitLabToken) {
    return laceworkGitLabToken
  }
  console.error("Neither LW_GITLAB_TOKEN nor LACEWORK_GITLAB_TOKEN environment variables found.")
  process.exit(0)
}

export async function callLaceworkCli(...args: string[]) {
    const accountName = getRequiredEnvVariable('LW_ACCOUNT')
    const apiKey = getRequiredEnvVariable('LW_API_KEY')
    const apiSecret = getRequiredEnvVariable('LW_API_SECRET')
    const expandedArgs = [
      '--noninteractive',
      '--account',
      accountName,
      '--api_key',
      apiKey,
      '--api_secret',
      apiSecret,
      ...args,
    ]
    return await callCommand('lacework', ...expandedArgs)
}

function redactApiCredentials(inputString: string): string {
  const redactedString = inputString.replace(/(?<=\s--api_key\s)(\S+)/g, "REDACTED")
                                    .replace(/(?<=\s--api_secret\s)(\S+)/g, "REDACTED");
  return redactedString;
}

export function getBranch(): string {
  return getRequiredEnvVariable("CI_COMMIT_BRANCH");
}
