  import { readFileSync, existsSync } from 'fs'
import { Log } from 'sarif'
import { callLaceworkCli, getRequiredEnvVariable } from './util'

export async function printResults(tool: string, sarifFile: string, format: string) {
    console.info(`Results for ${tool}`)
    let foundSomething = false

    if (format === 'sarif') {
      const results: Log = JSON.parse(readFileSync(sarifFile, 'utf8'))
      for (const run of results.runs) {
        if (Array.isArray(run.results) && run.results.length > 0) {
          foundSomething = true
          console.info('Found ' + run.results?.length + ' results using ' + tool)
          for (const vuln of run.results) {
            console.info(JSON.stringify(vuln, null, 2))
          }
        }
      }
    }
    if (format === 'gitlab-json') {
      const results = JSON.parse(readFileSync(sarifFile, 'utf-8'))
      if (Array.isArray(results.vulnerabilities) && results.vulnerabilities.length > 0) {
        foundSomething = true
        console.info(`Found ${results.vulnerabilities.length} results using ${tool}`)
        for(const vuln of results.vulnerabilities) {
          console.info(JSON.stringify(vuln, null, 2))
        }
      }
    }
    
    if (!foundSomething) {
      console.info(`No ${tool} issues were found`)
    }
}

export async function compareResults(
  tool: string,
  oldReport: string,
  newReport: string
): Promise<string> {
  console.info(`Comparing ${tool} results`)
  const args = [
    tool,
    'compare',
    '--old',
    oldReport,
    '--new',
    newReport,
    '--markdown',
    `${tool}.md`,
    '--link',
    // https://gitlab.com/d7834/WebGoat/blob/main/.gitlab-ci.yml#L4 - example URL slug pointing to file & line number
    `${getRequiredEnvVariable('CI_PROJECT_URL')}/blob/${getRequiredEnvVariable('CI_COMMIT_SHA')}/$FILENAME#L$LINENUMBER`,
    '--markdown-variant',
    'CommonMark',
    '--deployment',
    'ci',
    '--no-scr'
  ]
  if (process.env.LW_DEBUG === "true") args.push('--debug')
  await callLaceworkCli(...args)
  return existsSync(`${tool}.md`) ? readFileSync(`${tool}.md`, 'utf8') : ''
}