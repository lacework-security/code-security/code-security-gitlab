#!/bin/bash

echo "Starting scan.sh script"

if [[ ! -f /ci_runned ]] ; then
  if [[ $LW_TOOLS == *"sca"* ]]; then
    echo "Installing Lacework Code Security - SCA component"
    lacework --noninteractive -a $LW_ACCOUNT -k $LW_API_KEY -s $LW_API_SECRET component install sca --version $SCA_VERSION
  fi

  # Add a conditional check to see if SAST is included in $LW_TOOLS to save on pipeline execution time
  if [[ $LW_TOOLS == *"sast"* ]]; then
    echo "Installing Lacework Code Security - SAST component"
    lacework --noninteractive -a $LW_ACCOUNT -k $LW_API_KEY -s $LW_API_SECRET component install sast --version $SAST_VERSION
  fi

  # Echo LW cli and component versions
  lacework --noninteractive -a $LW_ACCOUNT -k $LW_API_KEY -s $LW_API_SECRET version
  lacework --noninteractive -a $LW_ACCOUNT -k $LW_API_KEY -s $LW_API_SECRET component list

  # Adding gitlab.com to known hosts
  # mkdir ~/.ssh && touch ~/.ssh/known_hosts && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts

  echo "Scanning $CI_PROJECT_PATH with contents:"
  echo "CI_PROJECT_PATH: $CI_PROJECT_PATH"
  ls -al /builds/$CI_PROJECT_PATH
  cd /builds/$CI_PROJECT_PATH

  node /app/dist/index.js
fi

[[ $CI ]] && touch /ci_runned

exec "$@"
