import { readFileSync } from 'fs';
import simpleGit, { SimpleGitOptions } from "simple-git";
import { LWJSON } from './lwjson';
import { callLaceworkCli, getBranch, getGitLabToken, getRequiredEnvVariable } from './util';
import { Gitlab } from '@gitbeaker/rest';

// Autofix 
const gitlab = new Gitlab({
    token: getGitLabToken(),
});

export const options: Partial<SimpleGitOptions> = {
    baseDir: process.cwd(),
    binary: 'git',
    maxConcurrentProcesses: 6,
    trimmed: false,
}
export async function checkLatestCommitSCA(): Promise<boolean> {
  const git = simpleGit(options)
  const logSummary = await git.log({ n: 1 }); // Retrieve the latest commit in the current branch
  const latestCommit = logSummary.latest;
  if(latestCommit) {
    if(latestCommit.message.startsWith("Fix for: codesec/sca/") || getBranch().startsWith("codesec/sca/")) {
      return true
    }
    return false
  } else {
    return false
  }
}
export async function createPRs(jsonFile: string) {
  const git = simpleGit(options)

  const repoName = getRequiredEnvVariable("CI_PROJECT_NAME")
  const projectNamespace = getRequiredEnvVariable("CI_PROJECT_NAMESPACE")
  // authenticate
  await git.raw(['remote', 'set-url', 'origin', `https://${repoName}:${getGitLabToken()}@gitlab.com/${projectNamespace}/${repoName}.git`])
  // parse lwjson SBOM  
  const results: LWJSON = JSON.parse(readFileSync(jsonFile, 'utf-8'))
  if (results.FixSuggestions == undefined) {
    return
  }
  // create PRs only if latest commit is not ours
  var response = await checkLatestCommitSCA() 
  if(response == true) {
    return
  }
  for (const fix of results.FixSuggestions) {
    let fixId: string = fix.FixId
    await prForFixSuggestion(jsonFile, fixId)
    }
}
export async function prForFixSuggestion(jsonFile: string, fixId: string) {
  let newBranch: string = 'codesec/sca/'
  // git configuration
  const git = simpleGit(options)
  await git.addConfig('user.name', 'Lacework Code Security', false, 'global')
  await git.addConfig('user.email', 'support@lacework.net', false, 'global')
  // get current branch
  // trigger: on push
  let currBranch = getBranch()
  newBranch += currBranch + '/'
  var patchReport = 'patchSummary.md'
  // create command to run on branch
  var args = ['sca', 'patch', '.', '--sbom', jsonFile, '--fix-id', fixId, '-o', patchReport]
  // call patch command
  await callLaceworkCli(...args)
  let patch = readFileSync(patchReport, 'utf-8')
  // title is the first line of the patch summary
  let titlePR = patch.split('\n')[0].substring(2)
  newBranch += titlePR.split('bump ')[1].split(' to')[0].replaceAll(' ', '_').replaceAll(':', '-')
  if (newBranch[newBranch.length - 1] == '.') {
    newBranch = newBranch.substring(0, newBranch.length - 1)
  }
  // create local branch
  await git.checkoutLocalBranch(newBranch)
  console.info("Checked out to branch " + newBranch)
  // parse the modified files from the patch summary
  let files: string[] = []
  let text: string = patch.split('## Files that have been modified:')[1]
  if (text == undefined) {
    return
  }
  let lines: string[] = text.split('\n')
  for (let line of lines) {
    // delete whitespaces
    line = line.trimStart().trimEnd()
    // delete the '*' and '-'
    line = line.substring(3, line.length - 1)
    files.push(line)
  }
  // add modified files to branch
  for (const file of files) {
    if (file != '') {
      await git.add(file)
    }
  }
  // commit and push changes --force to overwrite remote branch
  await git.commit('Fix for: ' + newBranch + '.').push('origin', newBranch, ['--force'])
  
  // check for existing PR and update if found
  let found = await isOpen(titlePR, newBranch, currBranch, patch) 
  // open PR
  if (!found) {
    await openPR(titlePR, newBranch, currBranch, patch)
  }
  await git.checkout(currBranch)
  console.info("Checked out to current branch.")
}

export async function openPR(titlePR: string, srcBranch: string, dstBranch: string, description: string) {
    try {
        var projectId = getRequiredEnvVariable('CI_PROJECT_ID')
        const mergeRequest = await gitlab.MergeRequests.create(
          projectId,
          srcBranch,
          dstBranch,
          titlePR,
          {description: description}
        );
    
        console.log('Merge Request Created:', mergeRequest.web_url)
      } catch (error) {
        console.error('Error creating Merge Request:', error)
      }
}

export async function isOpen(newTitle: string, srcBranch: string, dstBranch: string, newDescription: string): Promise<boolean>{
    try {
        // Search for MRs with the specified source and target branches
        var projectId = getRequiredEnvVariable('CI_PROJECT_ID')
        const mergeRequests = await gitlab.MergeRequests.all({projectId})
    
        // Check if any of the existing merge requests match the criteria
        const existingMR = mergeRequests.find(
            (mr) =>
                mr.source_branch === srcBranch &&
                mr.target_branch === dstBranch &&
                mr.state === 'opened'
        );
        if (existingMR) {
            await gitlab.MergeRequests.edit(projectId, existingMR.iid, {
                title: newTitle,
                description: newDescription
            })
            return true
        }
      } catch (error) {
        console.warn('Error checking for Merge Requests:', error);
      }
      return false
}

export async function updatePR(titlePR: string, srcBranch: string, dstBranch: string, description: string) {
    try {
        var projectId = getRequiredEnvVariable('CI_PROJECT_ID')
        const mergeRequest = await gitlab.MergeRequests.create(
          projectId,
          srcBranch,
          dstBranch,
          titlePR,
          {description: description}
        );
    
        console.log('Merge Request Created:', mergeRequest.web_url)
      } catch (error) {
        console.error('Error creating Merge Request:', error)
      }
}