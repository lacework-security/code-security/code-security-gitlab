<img src="https://techally-content.s3-us-west-1.amazonaws.com/public-content/lacework_logo_full.png" width="600">

# Lacework Code Security for GitLab Pipelines

This repository contains a GitLab Pipeline for using Lacework's code security offering on your code. In order for the code in this repository to run, you must be a Lacework customer that has been opted into the beta of our code security program. Please contact Lacework support for more information.

## Usage

### Creating secrets

Before attempting to run this action, you should add three secrets `LW_ACCOUNT_NAME`, `LW_API_KEY` and `LW_API_SECRET` to your GitHub repository (or, better yet, your GitLab organization so they can be shared accross all your repositories). The value for these secrets can be obtained by following the instructions [here](https://docs.lacework.com/console/api-access-keys) to create an API key and then download it.

## On pushes

To run an analysis on merge requests or pushes create a file called `.gitlab-ci-code-analysis.yml` with this content:

```yaml
include:
  - remote: 'https://gitlab.com/lacework-security/code-security/code-security-gitlab/-/raw/main/lacework-code-security.yaml'

stages:
  - security-scan

variables: 
  LW_AUTOFIX: "true" # To enable autofix
  LW_BRANCH: "custom_branch" # to change the branch you run the tool on
```

## Variables

### Autofix

To enable our autofix feature that will suggest fixes for vulnerabilities found by SCA on your main branch. To do so you need to set the variable to true, just like shown in the previous example. The default value for the variable is false.

### Branch to run the analysis on 

You can pick the branch you want to run SCA on with the LW_BRANCH environmental variable. If not specified, the default behaviour is to only run the tool on the branch "main". 

## License

The code contained in this repository is released as open-source under the Apache 2.0 license. However, the underlying analysis tools are subject to their own licensing conditions. Thus, you will not be able to use the code found here without having purchased the Lacework code security offering.